<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMstStoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_stories', function (Blueprint $table) {
            $table->id();
            $table->enum('kategori', ['nasional','daerah']);
            $table->string('judul')->nullable();
            $table->string('nama_pahlawan')->nullable();
            $table->bigInteger('area_id')->unsigned()->nullable();
            $table->foreign('area_id')->references('id')->on('mst_areas')->onDelete('set null');
            $table->boolean('is_validasi')->default(false);
            $table->boolean('is_aktif')->default(false);
            $table->text('cerita')->nullable();
            $table->text('audio')->nullable();
            $table->text('gambar')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_stories');
    }
}
