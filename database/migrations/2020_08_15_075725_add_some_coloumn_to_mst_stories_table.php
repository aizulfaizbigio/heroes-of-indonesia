<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSomeColoumnToMstStoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mst_stories', function (Blueprint $table) {
            $table->string('nama_contributor')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mst_stories', function (Blueprint $table) {
            if (Schema::hasColumn('mst_stories', 'nama_contributor')) {
                $table->dropColumn('nama_contributor');
            }
            if (Schema::hasColumn('mst_stories', 'created_at')) {
                $table->dropColumn('created_at');
            }
            if (Schema::hasColumn('mst_stories', 'updated_at')) {
                $table->dropColumn('updated_at');
            }
        });
    }
}
