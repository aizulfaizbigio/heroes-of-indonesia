@extends('homebase')
@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/material-components-web/4.0.0/material-components-web.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.21/css/dataTables.material.min.css" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="{{ asset("materialize/css-custom/custom.css") }}" media="screen,projection" />
<style>
  .mdc-data-table {
    display: block;
  }

  .mdc-button {
    color: #00b0ff !important;
  }

  .mdc-button--raised:not(:disabled),
  .mdc-button--unelevated:not(:disabled) {
    color: var(--mdc-theme-on-primary, #fff) !important;
    background-color: #00b0ff !important;
  }

  .mdc-button:disabled {
    color: rgba(0, 0, 0, .37) !important;
  }

  div.dataTables_wrapper .mdc-data-table__header-cell {
    border: 1px solid rgba(0, 0, 0, 0.12) !important;
  }
</style>
@endsection

@section('content')
<div class="row" style="margin-bottom: 5px">
  <div class="nav-wrapper right">
    <div class="col s12">
      <a href="#!" class="breadcrumb">{{ $parent_dashboard }}</a>
      <a href="#!" class="breadcrumb">Manajemen Kuis</a>
      <a href="#!" class="breadcrumb">{{ $title }}</a>
    </div>
  </div>
</div>
<div class="card-panel">
  <div class="row">
    <div class="input-field col s3">
      <a href="/kuis/tambah" class="btn btn-large waves-effect waves-light" type="button">
        Add <i class="material-icons right">add</i>
      </a>
    </div>
    <!-- <div class="input-field col s3 push-s6">
      <i class="material-icons prefix">search</i>
      <input id="icon_search" type="tel" class="validate">
      <label for="icon_search">Search Nama Kontributor</label>
    </div> -->
  </div>
  <div class="row">
    <div class="col s12">
      <table id="example" class="mdl-data-table cell-border" style="width:100%">
        <thead>
          <tr>
            <th>No</th>
            <th>Kategori</th>
            <th>Pertanyaan</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>System Architect</td>
            <td>61</td>
            <td>
              <a class='dropdown-trigger btn' href='#' data-target='dropdown1'>...</a>
              <ul id='dropdown1' class='dropdown-content'>
                <li><a href="#!">Edit</a></li>
                <li class="divider" tabindex="-1"></li>
                <li><a href="#!" onclick="deleteQuiz(this, 1)">Hapus</a></li>
              </ul>
            </td>
          </tr>
          <tr>
            <td>2</td>
            <td>Accountant</td>
            <td>63</td>
            <td>
              <a class='dropdown-trigger btn' href='#' data-target='dropdown1'>...</a>
              <ul id='dropdown1' class='dropdown-content'>
                <li><a href="#!">Edit</a></li>
                <li class="divider" tabindex="-1"></li>
                <li><a href="#!" onclick="deleteQuiz(this, 1)">Hapus</a></li>
              </ul>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/dataTables.material.min.js"></script>
<script>
  $(document).ready(function() {
    dataTable = $('#example').DataTable({
      processing: true,
      autoWidth: false,
      // serverSide: true,
      bLengthChange: false,
      searching: false,
      responsive: true,
      bSort: false,
      language: {
        emptyTable: "Data tidak tersedia",
        zeroRecords: "Tidak ada data yang ditemukan",
        infoFiltered: "",
        infoEmpty: "",
        paginate: {
          previous: "‹",
          next: "›"
        },
        info: "Menampilkan _START_ sampai _END_ dari _TOTAL_ Data Quiz",
        aria: {
          paginate: {
            previous: "Previous",
            next: "Next"
          }
        }
      },
      ajax: {
        url: 'http://hoi.bigio.id/api/quiz/datatable',
        type: "POST",
        data: {
          length: 9999,

          kategori: ''
        },
      },
      columns: [{
          data: null,
        },
        {
          data: "kategori",
        },
        {
          data: "judul"
        },
        {
          "data": {
            id: "id"
          }
        }
      ],
      columnDefs: [{
          "width": "5%",
          "targets": 0,
          createdCell: function(td, cellData, rowData, row, col) {
            $(td).html(dataTable.page.info().start + row + 1);
          }
        },
        {
          "width": "20%",
          "targets": 1,
          createdCell: function(td, cellData, rowData, row, col) {

          }
        },
        {
          "width": "20%",
          "targets": 2,
          createdCell: function(td, cellData, rowData, row, col) {

          }
        },
        {
          "width": "5%",
          "targets": 3,
          createdCell: function(td, cellData, rowData, row, col) {
            var id = cellData.id;
            var html = '<button class="waves-effect red darken-3 btn" onclick="deleteQuiz(this, ' + id + ')" data-judul="Pahlawan Trip"><i class="material-icons">delete_forever</i></button>';
            $(td).addClass("text-center");
            $(td).html(html);
          }
        },
      ],
    });
  });

  function deleteQuiz(event, id_quiz) {
    swal({
        title: `Apakah yakin anda menghapus data ini?`,
        text: "Data yang dihapus tidak bisa dikembalikan!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          console.log(id_quiz);
          $.ajax({
            type: "POST",
            url: "http://hoi.bigio.id/api/quiz/delete",
            data: {
              id: id_quiz
            },
            success: function(response) {
              swal(`Kuis Berhasil Dihapus! `, {
                icon: "success",
              });
              dataTable.ajax.reload();
            },
            error: function(xhr, ajaxOptions, thrownError) {
              swal("Data Gagal Dihapus!");
            }
          });
        } else {
          swal("Datamu Aman!");
        }
      });
  }
</script>
@endsection