@extends('homebase')
@section('css')
<link type="text/css" rel="stylesheet" href="{{ asset("materialize/css-custom/custom.css") }}" media="screen,projection" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<link type="text/css" rel="stylesheet" href="{{ asset("materialize/css/select2-materialize.css") }}" media="screen,projection" />
<style>
  .verified {
    font-size: 12pt;
    line-height: 0px !important;
  }

  .select2-container {
    height: 46px;
  }
</style>
@endsection

@section('content')
<div class="row" style="margin-bottom: 5px">
  <div class="nav-wrapper right">
    <div class="col s12">
      <a href="/" class="breadcrumb">{{ $parent_dashboard }}</a>
      <a href="/kisah" class="breadcrumb">Manajemen Kuis</a>
      <a href="#!" class="breadcrumb">{{ $title }}</a>
    </div>
  </div>
</div>
<div class="card">
  <form name="form-add-quiz" enctype="multipart/form-data">
    <div class="card-content">
      <span class="card-title">Tambah Kuis</span>
      <div class="row" style="margin-bottom: 0">
        <div class="input-field col s6">
          <select id="kategori" name="kategori">
            <option value="" disabled selected>Pilih Kategorinya dulu yaa :)</option>
            <option value="gambar">Tebak Nama Pahlawan</option>
            <option value="teks">Tebak Gambar Pahlawan</option>
          </select>
          <label>Kategori</label>
        </div>
      </div>
      <div class="row tebak-nama-container" style="margin-bottom: 0; display:none">
        <div class="input-field col s12">
          <input placeholder="Masukkan Pertanyaan Dulu Yaa :)" id="question" type="text" class="validate" name="judul">
          <label for="judul" class="active">Pertanyaan</label>
        </div>
      </div>
      <div class="row tebak-nama-container" style="margin-bottom: 0; display:none">
        <div class="col s6">
          <label for="question" class="active">Jawaban Benar</label>
          <div class="file-field input-field">
            <div class="btn">
              <span>File Gambar</span>
              <input type="file" id="jawaban-nama-benar" name="jawaban-nama-benar">
            </div>
            <div class="file-path-wrapper">
              <input class="file-path validate" type="text">
            </div>
          </div>
        </div>
        <div class="col s6">
          <label for="question" class="active">Jawaban Salah</label>
          <div class="file-field input-field">
            <div class="btn">
              <span>File Gambar</span>
              <input type="file" id="jawaban-nama-salah" name="jawaban-nama-salah">
            </div>
            <div class="file-path-wrapper">
              <input class="file-path validate" type="text">
            </div>
          </div>
        </div>
      </div>

      <div class="row tebak-gambar-container" style="margin-bottom: 0; display:none">
        <div class="col s6">
          <label for="answer" class="active">Pertanyaan</label>
          <div class="file-field input-field">
            <div class="btn">
              <span>File Gambar</span>
              <input type="file" id="gambar_soal" name="gambar_soal" accept="image/png, image/jpeg, image/jpg">
            </div>
            <div class="file-path-wrapper">
              <input class="file-path validate" type="text">
            </div>
          </div>
        </div>
      </div>
      <div class="row tebak-gambar-container" style="margin-bottom: 0; display:none">
        <div class="input-field col s6">
          <input placeholder="Masukkan Jawaban Dulu Yaa :)" id="jawaban-gambar-benar" type="text" class="validate" name="jawaban-gambar-benar">
          <label for="answer" class="active">Jawaban Benar</label>
        </div>
        <div class="input-field col s6">
          <input placeholder="Masukkan Jawaban Dulu Yaa :)" id="jawaban-gambar-salah" name="jawaban-gambar-salah" type="text" class="validate">
          <label for="answer" class="active">Jawaban Salah</label>
        </div>
      </div>
    </div>
  </form>
  <div class="card-action center">
    <a href="<?= url('/kuis') ?>" type="button" class="modal-close waves-effect waves-green btn red">Batal<i class="material-icons right">close</i></a>
    <button type="submit" id="add-button" class="waves-effect waves-green btn">Tambah<i class="material-icons right">add</i></button>
  </div>
</div>
@endsection

@section('js')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script type="text/javascript" src="{{ asset("materialize/js-custom/quiz/add.js") }}"></script>
@endsection