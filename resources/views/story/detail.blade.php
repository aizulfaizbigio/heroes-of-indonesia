@extends('homebase')
@section('css')
  <link type="text/css" rel="stylesheet" href="{{ asset("materialize/css-custom/custom.css") }}"  media="screen,projection"/>
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
  <link type="text/css" rel="stylesheet" href="{{ asset("materialize/css/select2-materialize.css") }}"  media="screen,projection"/>
  <style>
    .verified{
      font-size: 12pt;
      line-height: 0px !important;
    }
    .select2-container{
      height: 46px;
    }
  </style>
@endsection

@section('content')
  <div class="row" style="margin-bottom: 5px">
    <div class="nav-wrapper right">
      <div class="col s12">
        <a href="/" class="breadcrumb">{{ $parent_dashboard }}</a>
        <a href="/kisah" class="breadcrumb">Manajemen Kisah</a>
        <a href="#!" class="breadcrumb">{{ $title }}</a>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-content">
      <span class="card-title">Detail Kisah Pahlawan</span>
      <div class="row" style="margin-bottom: 0">
        <div class="input-field col s8">
          <input placeholder="Masukkan Nama Pahlawan :)" id="title" type="text" class="validate" readonly value="{{$data_story->judul}}">
          <label for="title" class="active">Judul</label>
        </div>
        <div class="input-field col s4">
          <select id="status" disabled>
            <option value="0" {{$data_story->is_aktif != 1? 'selected': ''}}>Tidak Aktif</option>
            <option value="1" {{$data_story->is_aktif == 1? 'selected': ''}}>Aktif</option>
          </select>
          <label>Status</label>
        </div>
      </div>
      <div class="row" style="margin-bottom: 0">
        <div class="input-field col s12">
          <input placeholder="Masukkan Nama Pahlawan :)" id="title" type="text" class="validate" readonly value="{{$data_story->nama_pahlawan}}">
          <label for="title" class="active">Nama Pahlawan</label>
        </div>
      </div>
      <div class="row" style="margin-bottom: 0">
        <div class="input-field col s6">
          <select id="category" disabled>
            <option value="1" {{$data_story->kategori == 'nasional'? 'selected': ''}}>Nasional</option>
            <option value="2" {{$data_story->kategori == 'daerah'? 'selected': ''}}>Daerah</option>
          </select>
          <label>Kategori</label>
        </div>
        <div class="input-field col s6" style="{{$data_story->kategori == 'daerah'? '': 'display:none'}}" id="region-container">
          <input placeholder="Masukkan Nama Daerah" id="region" type="text" class="validate" readonly value="{{empty($data_story->area_data)? "": $data_story->area_data->area_name}}">
          <label for="region" class="active">Daerah</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12" style="margin-top: 0">
          <textarea id="textarea1" class="materialize-textarea" readonly>{{$data_story->cerita}}</textarea>
          <label for="textarea1">Tuliskan Ceritamu :)</label>
        </div>
      </div>
      <div class="row">
        <div class="col s6">
          <div class="file-field input-field">
            <div class="btn">
              <span>File Gambar</span>
              <input type="file" disabled>
            </div>
            <div class="file-path-wrapper">
              <input class="file-path validate" type="text" readonly value="{{$data_story->gambar}}">
            </div>
          </div>
        </div>
        <div class="col s6">
          <div class="file-field input-field">
            <div class="btn">
              <span>File Suara (Jika Ada)</span>
              <input type="file" disabled>
            </div>
            <div class="file-path-wrapper">
              <input class="file-path validate" type="text" readonly value="{{$data_story->audio}}">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('js')
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
  <script type="text/javascript" src="{{ asset("materialize/js-custom/story/add.js") }}"></script>
@endsection