@extends('homebase')
@section('css')
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
  <link type="text/css" rel="stylesheet" href="{{ asset("materialize/css-custom/custom.css") }}"  media="screen,projection"/>
  <link type="text/css" rel="stylesheet" href="{{ asset("materialize/css/select2-materialize.css") }}"  media="screen,projection"/>
  <style>
    .verified{
      font-size: 12pt;
      line-height: 0px !important;
    }
    .select2-container{
      height: 46px;
    }

    .select2-selection, .select2-container{
      background-color: #FAFAFA !important;
    }

    p.short-desc{
      overflow: hidden;
      display: -webkit-box;
      -webkit-line-clamp: 1;
      -webkit-box-orient: vertical;
      text-overflow: ellipsis;
    }
  </style>
@endsection

@section('content')
  <div class="row" style="margin-bottom: 5px">
    <div class="nav-wrapper right">
      <div class="col s12">
        <a href="#!" class="breadcrumb">{{ $parent_dashboard }}</a>
        <a href="#!" class="breadcrumb">Manajemen Kisah</a>
      </div>
    </div>
  </div>
  <div class="card-panel">
    <div class="row" style="margin-bottom: 0">
      <div class="input-field col s3">
        <a href="/kisah/tambah" class="btn btn-large waves-effect waves-light" type="button">
          Add <i class="material-icons right">add</i>
        </a>
      </div>
      <div class="input-field col s3 push-s3">
        <select id="filter-category">
          <option value="" selected>Semua Kategori</option>
          <option value="nasional">Nasional</option>
          <option value="daerah">Daerah</option>
        </select>
        <label>Kategori</label>
      </div>
      <div class="input-field col s3 push-s3">
        <input id="filter-search" type="text" class="validate" placeholder="Search Nama Kontributor">
        <label for="filter-search">Search</label>
      </div>
    </div>
    <div class="row" id="card-container">
    </div>
    <div class="row pagination">
      <ul class="pagination right" id="pagination">
      </ul>
    </div>
  </div>
  
<!-- components -->
<div class="col m12 l4 hide" id="story-card-clonable">
  <div class="col m12 l4">
    <div class="card sticky-action">
      <div class="card-image waves-effect waves-block">
        <img class="activator logo" src="" style="height: 388px; width: 388px; overflow: hidden;">
      </div>
      <div class="card-content">
        <span class="card-title activator grey-text text-darken-4 card-header">
        </span>
        <p class="short-desc"></p>
      </div>
      <div class="card-reveal">
        <span class="card-title grey-text text-darken-4 card-header-detail"></span>
        <p class="long-desc"></p>
      </div>
      <div class="card-action" style="text-align: right">
        <div class="switch left" style="margin-top: 5px">
          <label>
            <input onclick="" class="validating" type="checkbox">
            <span class="lever"></span>
            Validate
          </label>
        </div>
        <div class="buttons">
          <button class="waves-effect red darken-3 btn btn-delete-card" onclick="" data-judul=""><i class="material-icons">delete_forever</i></button>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twbs-pagination/1.4.2/jquery.twbsPagination.min.js"></script>
  <script type="text/javascript" src="{{ asset("materialize/js-custom/story/index.js") }}"></script>
  <script>
    var DOMAIN = "{{ asset("upload/gambar/")}}/";
    var DEFAULT = "{{ asset("image/default/foto_def.png")}}";
  </script>
@endsection