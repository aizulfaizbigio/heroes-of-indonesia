@extends('homebase')
@section('css')
  <link type="text/css" rel="stylesheet" href="{{ asset("materialize/css-custom/custom.css") }}"  media="screen,projection"/>
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
  <link type="text/css" rel="stylesheet" href="{{ asset("materialize/css/select2-materialize.css") }}"  media="screen,projection"/>
  <style>
    .verified{
      font-size: 12pt;
      line-height: 0px !important;
    }
    .select2-container{
      height: 46px;
    }
  </style>
@endsection

@section('content')
  <div class="row" style="margin-bottom: 5px">
    <div class="nav-wrapper right">
      <div class="col s12">
        <a href="/" class="breadcrumb">{{ $parent_dashboard }}</a>
        <a href="/kisah" class="breadcrumb">Manajemen Kisah</a>
        <a href="#!" class="breadcrumb">{{ $title }}</a>
      </div>
    </div>
  </div>
  <div class="card">
    <form id="form-add-story">
      <div class="card-content">
        <span class="card-title">Tambah Kisah Pahlawan</span>
        <div class="row" style="margin-bottom: 0">
          <div class="input-field col s8">
            <input placeholder="Masukkan Judul" id="title" name="judul" type="text" class="validate">
            <label for="title" class="active">Judul</label>
          </div>
          <div class="input-field col s4">
            <select id="is_aktif" name="is_aktif">
              <option value="0">Tidak Aktif</option>
              <option value="1">Aktif</option>
            </select>
            <label>Status</label>
          </div>
        </div>
        <div class="row" style="margin-bottom: 0">
          <div class="input-field col s12">
            <input placeholder="Masukkan Nama Pahlawan :)" id="nama_pahlawan" name="nama_pahlawan" type="text" class="validate">
            <label for="nama_pahlawan" class="active">Nama Pahlawan</label>
          </div>
        </div>
        <div class="row" style="margin-bottom: 0">
          <div class="input-field col s6">
            <select id="kategori" name="kategori">
              <option value="nasional">Nasional</option>
              <option value="daerah">Daerah</option>
            </select>
            <label>Kategori</label>
          </div>
          <div class="input-field col s6" id="region-container" style="display: none">
            <select class="select-wrapper no-autoinit" id="area_id" name="area_id">
              <option value="">Masukkan Daerah Pahlawan</option>
            </select>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12" style="margin-top: 0">
            <textarea id="cerita" name="cerita" class="materialize-textarea"></textarea>
            <label for="cerita">Tuliskan Ceritamu :)</label>
          </div>
        </div>
        <div class="row">
          <div class="col s6">
            <div class="file-field input-field">
              <div class="btn">
                <span>File Gambar</span>
                <input type="file" id="gambar" name="gambar">
              </div>
              <div class="file-path-wrapper">
                <input class="file-path validate" type="text">
              </div>
            </div>
          </div>
          <div class="col s6">
            <div class="file-field input-field">
              <div class="btn">
                <span>File Suara (Jika Ada)</span>
                <input type="file" id="audio" name="audio">
              </div>
              <div class="file-path-wrapper">
                <input class="file-path validate" type="text">
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="card-action center">
        <a href="/kisah" class="modal-close waves-effect waves-green btn red">Batal<i class="material-icons right">close</i></a>
        <button type="submit" id="add-button" class="waves-effect waves-green btn">Tambah<i class="material-icons right">add</i></button>
      </div>
    </form>
  </div>
@endsection

@section('js')
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
  <script type="text/javascript" src="{{ asset("materialize/js-custom/story/add.js") }}"></script>
@endsection