<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>heroQ</title>
  <link rel="icon" type="image/png" href="{{ asset("image/default/logo.png") }}">

  <!--Import Google Icon Font-->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="{{ asset("materialize/css/materialize.css") }}" media="screen,projection" />
  <link type="text/css" rel="stylesheet" href="{{ asset("materialize/css-custom/main.css") }}" media="screen,projection" />
  <link type="text/css" rel="stylesheet" href="{{ asset("materialize/css-custom/sidebar.css") }}" media="screen,projection" />
  <link type="text/css" rel="stylesheet" href="{{ asset("materialize/css-custom/custom.css") }}" media="screen,projection" />

  <!-- Custom Css -->
  @yield('css')
</head>

<body>
  <!-- Main Content -->
  <div class="section"></div>
  <main style="padding: 0px;">
    <div class="center-align">
      <img class="responsive-img" style="width: 250px;" src="{{ asset("image/default/logo.png") }}" />
      <div class="section"></div>

      <div class="container">
        <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 48px 32px 48px; border: 1px solid #EEE; border-radius: 20px;">

          <form class="col s12" method="POST" action="{{ route('login') }}" style="width: 350px;">
            @csrf
            <div class='row'>
              <div class='col s12'>
              </div>
            </div>

            <div class='row'>
              <div class='input-field col s12'>
                <input class='validate' type='email' name='email' id='email' />
                <label for='email'>Enter your email</label>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>
            </div>

            <div class='row'>
              <div class='input-field col s12'>
                <input class='validate' type='password' name='password' id='password' />
                <label for='password'>Enter your password</label>
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>
            </div>

            <div class='row' style="display: none">
              <div class='input-field col s12'>
                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                <label class="form-check-label" for="remember">
                    {{ __('Remember Me') }}
                </label>
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>
            </div>

            <br />
            <center>
              <div class='row'>
                <button type='submit' name='btn_login' class='col s12 btn btn-large waves-effect' style="background-color: #00b0ff; border-radius: 10px;">Login</button>
              </div>
            </center>
          </form>
        </div>
      </div>

      Don't Have an Account? <a href="">Sign Up</a> Now

      <div class="section"></div>
      <div class="section"></div>
    </div>
  </main>

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>

  <!--JavaScript at end of body for optimized loading-->
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script type="text/javascript" src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script type="text/javascript" src="{{ asset("materialize/js/materialize.js") }}"></script>
  <script>
    $(document).ready(function() {
      $('#sidenav').sidenav({
        edge: 'left'
      });
      $('.collapsible').collapsible();
    });
  </script>

  <!-- Custom JS -->
  @yield('js')
</body>

</html>