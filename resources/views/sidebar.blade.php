<aside>
    <ul id="sidenav" class="sidenav sidenav-fixed" style="transform: translateX(0px);">
        <li class="no-padding">
            <div class="logo" style="padding: 20px; padding-bottom: 0px; text-align: center;">
                <img class="background" src="{{ asset("image/default/logo.png") }}" style="width: 150px; height: 150px;">
            </div>
        </li>
        <div class="divider" id="divider-sidebar"></div>
        <li class="bold {{@$page_dashboard}}"><a href="/" class="waves-effect waves-blue" style="display: none">Dashboard</a></li>
        <li class="bold {{@$page_story}}"><a href="/kisah" class="waves-effect waves-blue">Kisah Pahlawan</a></li>
        <li class="bold {{@$page_quiz}}">
            <a href="/kuis" class="waves-effect waves-blue" tabindex="0" style="padding-left: 32px;">Kuis</a>
        </li>
    </ul>
</aside>