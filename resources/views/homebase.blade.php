<!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>ADMIN | Heroes Of Indonesia</title>
      <link rel="icon" type="image/png" href="{{ asset("image/default/logo.png") }}">

      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="{{ asset("materialize/css/materialize.css") }}"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="{{ asset("materialize/css-custom/main.css") }}"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="{{ asset("materialize/css-custom/sidebar.css") }}"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="{{ asset("materialize/css-custom/custom.css") }}"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="{{ asset("growl/css/jquery.growl.css") }}"/>

      <!-- Custom Css -->
      @yield('css')
    </head>

    <body>
        <!-- Header -->
        @include('header')

        <!-- Sidebar -->
        @include('sidebar')

        <!-- Main Content -->
        <main>
          <div class="container">
            <div class="row" style="margin-bottom: 0px; text-align: left;">
              <a class="waves-effect btn-flat" id="button-back">
                <strong style="font-weight: 700">{{ $page_title }}</strong>
              </a>
            </div>
            @yield('content')
          </div>
        </main>

        <!--JavaScript at end of body for optimized loading-->
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <script type="text/javascript" src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script type="text/javascript" src="{{ asset("growl/js/jquery.growl.js") }}"></script>
        <script type="text/javascript" src="{{ asset("materialize/js/materialize.js") }}"></script>
        <script>
          $(document).ready(function () {
            M.AutoInit();
            $('#sidenav').sidenav({ edge: 'left' });
            $('.collapsible').collapsible();
          });
        </script>

        <!-- Custom JS -->
        @yield('js')
    </body>
  </html>