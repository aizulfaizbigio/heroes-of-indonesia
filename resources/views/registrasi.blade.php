<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>heroQ</title>
  <link rel="icon" type="image/png" href="{{ asset("image/default/logo.png") }}">

  <!--Import Google Icon Font-->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="{{ asset("materialize/css/materialize.css") }}" media="screen,projection" />
  <link type="text/css" rel="stylesheet" href="{{ asset("materialize/css-custom/main.css") }}" media="screen,projection" />
  <link type="text/css" rel="stylesheet" href="{{ asset("materialize/css-custom/sidebar.css") }}" media="screen,projection" />
  <link type="text/css" rel="stylesheet" href="{{ asset("materialize/css-custom/custom.css") }}" media="screen,projection" />
  <link type="text/css" rel="stylesheet" href="{{ asset("growl/css/jquery.growl.css") }}"/>

  <!-- Custom Css -->
  @yield('css')
</head>

<body>
  <!-- Main Content -->
  <div class="section"></div>
  <main style="padding: 0px;">
    <div class="center-align">
      <img class="responsive-img" style="width: 250px;" src="{{ asset("image/default/logo.png") }}" />
      <div class="section"></div>

      <div class="container">
        <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 48px 32px 48px; border: 1px solid #EEE; border-radius: 20px;">

          <form class="col s12" method="post" style="width: 350px;" id="registrasi">
            <div class='row'>
              <div class='col s12'>
              </div>
            </div>

            <div class='row'>
              <div class='input-field col s12'>
                <div class="file-field input-field">
                  <div class="btn">
                    <span>File Suara (Jika Ada)</span>
                    <input type="file" id="audio" name="audio">
                  </div>
                  <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                  </div>
                </div>
              </div>
            </div>

            <br />
            <center>
              <div class='row'>
                <button type='submit' id='btn_login' class='col s12 btn btn-large waves-effect' style="background-color: #00b0ff; border-radius: 10px;">Login</button>
              </div>
            </center>
          </form>
        </div>
      </div>
      <div class="section"></div>
      <div class="section"></div>
    </div>
  </main>

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>

  <!--JavaScript at end of body for optimized loading-->
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script type="text/javascript" src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script type="text/javascript" src="{{ asset("materialize/js/materialize.js") }}"></script>
  <script type="text/javascript" src="{{ asset("growl/js/jquery.growl.js") }}"></script>
  <script>
    $(document).ready(function() {

      $("#audio").change((e) => {
        var formData = new FormData();
        var file = $("#audio").prop('files')[0];
        formData.append("audio", file);

        $.ajax({
            type: "post",
            url: "/api/upload-audio",
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $("#btn_login").attr("disabled", true);
            },
            success: function (response) {
                console.log(response);
                $.growl.notice({
                    message: "Anda Berhasil Registrasi!"
                });
                $("#btn_login").attr("disabled", false);
                location.replace("/");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $.growl.error({
                    message: "Anda Gagal Registrasi!"
                });
                $("#btn_login").attr("disabled", false);
            }
        });
      })
      // $('body').on('submit', '#registrasi', function (e) {
      //     e.preventDefault();

      //     var formData = new FormData(this);
      //     $.ajax({
      //         type: "post",
      //         url: "/api/registrasi",
      //         data: formData,
      //         contentType: false,
      //         processData: false,
      //         beforeSend: function () {
      //             $("#btn_login").attr("disabled", true);
      //         },
      //         success: function (response) {
      //             console.log(response);
      //             $.growl.notice({
      //                 message: "Anda Berhasil Registrasi!"
      //             });
      //             $("#btn_login").attr("disabled", false);
      //             location.replace("/");
      //         },
      //         error: function (xhr, ajaxOptions, thrownError) {
      //             $.growl.error({
      //                 message: "Anda Gagal Registrasi!"
      //             });
      //             $("#btn_login").attr("disabled", false);
      //         }
      //     });
      // });
    });
  </script>

  <!-- Custom JS -->
  @yield('js')
</body>

</html>