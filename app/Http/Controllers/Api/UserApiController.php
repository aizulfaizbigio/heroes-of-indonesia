<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseApiController;

use App\User;

use App\Http\Controllers\Api\QueryFilters\StoryQueryFilter;
use App\Http\Controllers\Api\AddForms\StoryAddForm;
use App\Http\Controllers\Api\AddForms\StoryContributateAddForm;

use Illuminate\Auth\Access\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;

class UserApiController extends BaseApiController
{
    public function create_user(Request $request)
    {
        Log::info(var_export($request->toArray(), TRUE));
        DB::beginTransaction();
        try {
            $dataUser['name'] = "CIMORI";
            $dataUser['email'] = "cimori@mail.com";
            $dataUser['password'] =  Hash::make("cimori123");
            $user_create = User::create($dataUser);

            DB::commit();

            return $this->success_response($user_create);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage() . ' | File: ' . $e->getFile() . ' | Line: ' . $e->getLine());

            return $this->error_response($e->getMessage());
        }
    }

    public function create_registration(Request $request)
    {
        $rulesUser = [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
        ];
        
        $validator = Validator::make($request->all(), $rulesUser);
        if ($validator->fails()){
            return response($validator->messages()->toArray(), 400);
        }

        Log::info(var_export($request->toArray(), TRUE));
        DB::beginTransaction();
        try {
            $dataUser = $request->all();
            $dataUser['password'] =  Hash::make($request->password);
            $user_create = User::create($dataUser);

            DB::commit();

            return $this->success_response($masjid_create);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage() . ' | File: ' . $e->getFile() . ' | Line: ' . $e->getLine());

            return $this->error_response($e->getMessage());
        }
    }
}
