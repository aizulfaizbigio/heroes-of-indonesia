<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseApiController;

use App\Model\MstQuestion;
use App\Model\MstAnswer;

use App\Http\Controllers\Api\QueryFilters\QuizQueryFilter;

use Illuminate\Auth\Access\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class QuizApiController extends BaseApiController
{
    public function datatable(Request $request)
    {
        $qf = new QuizQueryFilter($request->all());
        $query = MstQuestion::with(['answer_data']);

        if ($qf->get_search_text() != null) {
            $query = $query->where("judul", "like", "%{$qf->get_search_text()}%");
        }

        if ($qf->get_id() != null) {
            $query = $query->where("id", $qf->get_id());
        }

        if ($qf->get_kategori() != null) {
            $query = $query->where("kategori", $qf->get_kategori());
        }

        $count = $query->count();
        $result = $query->limit($qf->get_length())->offset($qf->get_start());

        $data = $this->set_datatable_response($qf->get_draw(), $count, $result->get());
        return $this->success_response_datatable($data);
    }

    public function getQuiz(Request $request)
    {
        $countRecords = MstQuestion::withTrashed()->count();
        // error_log('total records: ' . $countRecords);

        $dataQuiz = MstQuestion::with(['answer_data' => function ($q) {
            $q->inRandomOrder();
        }])->where('kategori', $request->kategori)->limit($request->length)->inRandomOrder()->get();
        if($dataQuiz == null){
            return $this->error_response("Gagal Mendapatkan Quiz!");
        }else{
            return $this->success_response($dataQuiz);
        }
    }

    public function validation(Request $request)
    {
        $rules = [
            'id' => 'required|exists:mst_question,id',
            'answer_id' => 'required|exists:mst_answer,id'
        ];

        $data_fields = $request->all();
        $validator = Validator::make($data_fields, $rules);

        if ($validator->fails()) {
            return response($validator->messages()->toArray(), 400);
        }

        $result = MstAnswer::where('question_id', $data_fields['id'])->where('id', $data_fields['answer_id'])->first();
        $jawaban = false;
        if($result->is_jawaban){
            $jawaban = true;
        }

        return $this->success_response($jawaban);
    }

    public function uploadImage(Request $request)
    {
        $rules = [
            'gambar' => 'required',
        ];

        $data_fields = $request->all();
        $validator = Validator::make($data_fields, $rules);

        if ($validator->fails()) {
            return response($validator->messages()->toArray(), 400);
        }

        Log::info(var_export($request->toArray(), TRUE));
        try {
            $file = $request->file('gambar');
            $image_name = time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('upload/gambar-quiz/'), $image_name);

            $result = [
                $image_name
            ];

            return $this->success_response($result);
        } catch (\Exception $e) {
            Log::error($e->getMessage() . ' | File: ' . $e->getFile() . ' | Line: ' . $e->getLine());

            return $this->error_response($e->getMessage());
        }
    }

    public function create(Request $request){
        $rules = [
            'kategori' => 'required|in:gambar,teks',
            'judul' => 'required',
            'gambar' => 'required',
            'answers.*.jawaban' => 'required',
            'answers.*.is_jawaban' => 'required',
        ];

        // return $this->success_response($request->all());

        $data_fields = $request->all();
        $validator = Validator::make($data_fields, $rules);

        if ($validator->fails()) {
            return response($validator->messages()->toArray(), 400);
        }

        Log::info(var_export($request->toArray(), TRUE));
        DB::beginTransaction();
        try {
            $question = MstQuestion::create($data_fields);
            foreach($data_fields['answers'] as $answer){
                $temp = [
                    'question_id' => $question->id,
                    'jawaban' => $answer['jawaban'],
                    'is_jawaban' => $answer['is_jawaban'],
                ];
                $answers[] = $temp;
            }

            MstAnswer::insert($answers);
            DB::commit();

            $result = [
                $question,
                'answer_data' => $answers
            ];

            return $this->success_response($result);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage() . ' | File: ' . $e->getFile() . ' | Line: ' . $e->getLine());

            return $this->error_response($e->getMessage());
        }
    }

    public function update(Request $request){
        $rules = [
            'id' => 'required|exists:mst_questions,id',
            'kategori' => 'required|in:gambar,teks',
            'judul' => 'required',
            'gambar' => 'required',
            'answers.*.jawaban' => 'required',
            'answers.*.is_jawaban' => 'required',
        ];

        $data_fields = $request->all();
        $validator = Validator::make($data_fields, $rules);

        if ($validator->fails()) {
            return response($validator->messages()->toArray(), 400);
        }

        Log::info(var_export($request->toArray(), TRUE));
        DB::beginTransaction();
        try {
            $question = MstQuestion::where('id', $data_fields['id'])->update($data_fields);
            MstAnswer::where('question_id', $data_fields['id'])->delete();

            foreach($data_fields['answers'] as $answer){
                $temp = [
                    'question_id' => $question->id,
                    'jawaban' => $answer['jawaban'],
                    'is_jawaban' => $answer['is_jawaban'],
                ];
                $answers[] = $temp;
            }

            MstAnswer::insert($answers);
            DB::commit();

            $result = [
                $question,
                'answer_data' => $answers
            ];

            return $this->success_response($result);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage() . ' | File: ' . $e->getFile() . ' | Line: ' . $e->getLine());

            return $this->error_response($e->getMessage());
        }
    }

    public function delete(Request $request){
        $rules = [
            'id' => 'required|exists:mst_questions,id',
        ];

        $data_fields = $request->all();
        $validator = Validator::make($data_fields, $rules);

        if ($validator->fails()) {
            return response($validator->messages()->toArray(), 400);
        }

        Log::info(var_export($request->toArray(), TRUE));
        DB::beginTransaction();
        try {
            MstQuestion::where('id', $data_fields['id'])->first()->delete();
            MstAnswer::where('question_id', $data_fields['id'])->first()->delete();
            DB::commit();

            return $this->success_response();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage() . ' | File: ' . $e->getFile() . ' | Line: ' . $e->getLine());

            return $this->error_response($e->getMessage());
        }
    }
}
