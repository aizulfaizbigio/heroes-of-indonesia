<?php

namespace App\Http\Controllers\Api\AddForms;

use App\Http\Controllers\Api\AddForms\BaseForm;

class StoryAddForm extends BaseForm{

    public function __construct($request){
        $this->fields = [
            [
                'name' => 'title',
                'label' => 'Judul', 
                'value' => value_from_request($request, 'title'),
                'validate' => true
            ],
            [
                'name' => 'description',
                'label' => 'Deskripsi', 
                'value' => value_from_request($request, 'description'),
                'validate' => false
            ],
            [
                'name' => 'type',
                'label' => 'Jenis', 
                'value' => value_from_request($request, 'type'),
                'validate' => true
            ]
        ];
    }

}