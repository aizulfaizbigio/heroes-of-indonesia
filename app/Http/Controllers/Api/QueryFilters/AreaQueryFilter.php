<?php

namespace App\Http\Controllers\Api\QueryFilters;

use App\Http\Controllers\Api\QueryFilters\BaseQueryFilter;

class AreaQueryFilter extends BaseQueryFilter{
    function __construct($request){
        parent::__construct($request);   
    }
}