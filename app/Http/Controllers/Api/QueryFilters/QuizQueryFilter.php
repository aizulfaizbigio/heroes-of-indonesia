<?php

namespace App\Http\Controllers\Api\QueryFilters;

use App\Http\Controllers\Api\QueryFilters\BaseQueryFilter;

class QuizQueryFilter extends BaseQueryFilter{

    private $kategori;

    function __construct($request){
        parent::__construct($request);
        $this->kategori = isset($request['kategori']) ? $request['kategori'] : null;
        
    }

    public function get_kategori(){
        return $this->kategori;
    }
}