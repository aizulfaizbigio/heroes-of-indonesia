<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Log;


class QuizController extends Controller
{
    public function __constuct()
    {
        parent::__constuct();
    }

    public function pageNameQuiz()
    {
        $data['page_title'] = 'Manajemen Kuis Tebak Nama';
        $data['page_description'] = 'Manajemen Kuis Tebak Nama';
        $data['page_quiz'] = 'active';
        $data['page_name_quiz'] = 'active';
        $parent_dashboard = 'Dashboard';
        $title = 'Quiz';
        return view('quiz.index', ['title' => $title, 'parent_dashboard' => $parent_dashboard, 'kategori' => 'nama'])->with($data);
    }

    public function pageImageQuiz()
    {
        $data['page_title'] = 'Manajemen Kuis Tebak Gambar';
        $data['page_description'] = 'Manajemen Kuis Tebak Gambar';
        $data['page_quiz'] = 'active';
        $data['page_image_quiz'] = 'active';
        $parent_dashboard = 'Dashboard';
        $title = 'Quiz';
        return view('quiz.index', ['title' => $title, 'parent_dashboard' => $parent_dashboard, 'kategori' => 'gambar'])->with($data);
    }

    public function create()
    {
        $data['page_title'] = 'Tambah Kuis';
        $data['page_description'] = 'Tambah Kuis';
        $data['page_quiz'] = 'active';
        $data['page_image_quiz'] = 'active';
        $parent_dashboard = 'Dashboard';
        $title = 'Tambah';
        return view('quiz.add', ['title' => $title, 'parent_dashboard' => $parent_dashboard])->with($data);
    }
}
