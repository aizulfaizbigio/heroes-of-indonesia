<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Log;
use App\Model\MstStory;
use App\Model\MstArea;

class StoryController extends Controller
{
    public function __constuct()
    {
        parent::__constuct();
    }

    public function index()
    {
        $data['page_title'] = 'Manajemen Kisah Pahlawan';
        $data['page_description'] = 'Manajemen Kisah Pahlawan';
        $data['page_story'] = 'active';
        $parent_dashboard = 'Dashboard';
        $title = 'index';
        return view('story.index', ['title' => $title, 'parent_dashboard' => $parent_dashboard])->with($data);
    }

    public function detail($id)
    {
        $data['page_title'] = 'Detail Kisah Pahlawan';
        $data['page_description'] = 'Detail Kisah Pahlawan';
        $data['page_story'] = 'active';
        $data['data_story'] = MstStory::with(['area_data'])->where('id', $id)->first();
        $parent_dashboard = 'Dashboard';
        $title = 'Detail';
        return view('story.detail', ['title' => $title, 'parent_dashboard' => $parent_dashboard])->with($data);
    }

    public function create()
    {
        $data['page_title'] = 'Tambah Kisah Pahlawan';
        $data['page_description'] = 'Tambah Kisah Pahlawan';
        $data['page_story'] = 'active';
        $data['page_region_story'] = 'active';
        $parent_dashboard = 'Dashboard';
        $title = 'Tambah';
        return view('story.add', ['title' => $title, 'parent_dashboard' => $parent_dashboard])->with($data);
    }

    public function update($id)
    {
        $data['page_title'] = 'Ubah Kisah Pahlawan';
        $data['page_description'] = 'Ubah Kisah Pahlawan';
        $data['page_story'] = 'active';
        $data['page_region_story'] = 'active';
        $data['data_story'] = MstStory::with(['area_data'])->where('id', $id)->first();
        $parent_dashboard = 'Dashboard';
        $title = 'Ubah';
        return view('story.edit', ['title' => $title, 'parent_dashboard' => $parent_dashboard])->with($data);
    }
}
