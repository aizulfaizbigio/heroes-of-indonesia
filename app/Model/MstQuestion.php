<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MstQuestion extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'kategori',
        'judul',
        'gambar',
        'deleted_at'
    ];

    public function answer_data()
    {
        // return $this->belongsTo('App\Model\MstArea', 'area_id', 'id');
        return $this->hasMany('App\Model\MstAnswer', 'question_id', 'id');
    }
}
