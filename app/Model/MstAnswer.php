<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MstAnswer extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'question_id',
        'jawaban',
        'is_jawaban',
    ];

    public function question_data()
    {
        return $this->hasMany('App\Model\MstQuestion', 'question_id', 'id');
        // return $this->belongsTo(MstAnswer::class);
    }
}
