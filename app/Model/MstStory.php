<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\MstArea;
use DateTimeInterface;

class MstStory extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'kategori',
        'judul',
        'nama_pahlawan',
        'nama_contributor',
        'area_id',
        'is_validasi',
        'is_aktif',
        'cerita',
        'audio',
        'gambar',
        'created_at_on_datetime',
        'deleted_at'
    ];

    protected function serializeDate(DateTimeInterface $date){
        // return $date->format('Y-m-d H:i:s');
        // return $date->toAtomString();
        return $date->toISOString();
    }

    public function area_data()
    {
        return $this->belongsTo('App\Model\MstArea', 'area_id', 'id');
    }
}
