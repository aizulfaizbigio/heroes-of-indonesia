<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::group(['middleware' => ['web', 'auth']], function () {
    Route::get('/', function () {
        return redirect('kisah');
    });
    // routes manajemen story
    Route::get('/kisah', 'StoryController@index')->name('kisah');
    Route::get('/kisah/detail/{id}', 'StoryController@detail');
    Route::get('/kisah/tambah', 'StoryController@create');
    Route::get('/kisah/ubah/{id}', 'StoryController@update');
});

Route::get('/login', function () {
    return view('login');
})->name('login');

Route::get('/registrasi', function () {
    return view('registrasi');
})->name('registrasi');

// routes manajemen quiz
Route::get('/kuis', 'QuizController@pageNameQuiz')->name('kuis');
Route::get('/kuis/tebak-nama', 'QuizController@pageNameQuiz')->name('kuis');
Route::get('/kuis/tebak-gambar', 'QuizController@pageImageQuiz')->name('kuis');
Route::get('/kuis/tambah', 'QuizController@create');

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');
