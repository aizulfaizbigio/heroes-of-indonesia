$(document).ready(function () {

  var gambar_soal = null;
  var jawaban_nama_salah = null;
  var jawaban_nama_benar = null;
  $('#kategori').on('change', (e) => {
    if ($("#kategori").val() == 'teks') {
      $('.tebak-nama-container').show()
      $('.tebak-gambar-container').hide()
    } else {
      $('.tebak-gambar-container').show()
      $('.tebak-nama-container').hide()
    }
  });

  $('#gambar_soal').change((e) => {
    e.preventDefault();

    var formData = new FormData();
    var file = $("#gambar_soal").prop('files')[0];
    formData.append('gambar', file);
    console.log(formData);

    $.ajax({
      type: "post",
      url: "http://hoi.bigio.id/api/quiz/uploadImage",
      data: formData,
      contentType: false,
      processData: false,
      success: function (response) {
        gambar_soal = response.data[0];
        $.growl.notice({
          message: "Data Soal berhasil Ditambahkan"
        });
      },
      error: function (xhr, ajaxOptions, thrownError) {
        $.growl.error({
          message: "Data Gagal Ditambahkan!"
        });
        $("#add-button").attr("disabled", false);
      }
    });
  });

  $('#jawaban-nama-benar').change((e) => {
    e.preventDefault();

    var formData = new FormData();
    var file = $("#jawaban-nama-benar").prop('files')[0];
    formData.append('gambar', file);
    console.log(formData);

    $.ajax({
      type: "post",
      url: "http://hoi.bigio.id/api/quiz/uploadImage",
      data: formData,
      contentType: false,
      processData: false,
      success: function (response) {
        jawaban_nama_benar = response.data[0];
        $.growl.notice({
          message: "Data Soal berhasil Ditambahkan"
        });
      },
      error: function (xhr, ajaxOptions, thrownError) {
        $.growl.error({
          message: "Data Gagal Ditambahkan!"
        });
        $("#add-button").attr("disabled", false);
      }
    });
  });

  $('#jawaban-nama-salah').change((e) => {
    e.preventDefault();

    var formData = new FormData();
    var file = $("#jawaban-nama-salah").prop('files')[0];
    formData.append('gambar', file);
    console.log(formData);

    $.ajax({
      type: "post",
      url: "http://hoi.bigio.id/api/quiz/uploadImage",
      data: formData,
      contentType: false,
      processData: false,
      success: function (response) {
        jawaban_nama_salah = response.data[0];
        $.growl.notice({
          message: "Data Soal berhasil Ditambahkan"
        });
      },
      error: function (xhr, ajaxOptions, thrownError) {
        $.growl.error({
          message: "Data Gagal Ditambahkan!"
        });
        $("#add-button").attr("disabled", false);
      }
    });
  });

  $("#add-button").click(function () {

    var kategori = $("#kategori").val();
    var judul = $("#kategori").val() == 'teks' ? $("#question").val() : "Siapakah Gambar Dibawah Ini?";
    var gambar = $("#kategori").val() == 'teks' ? "-" : gambar_soal;
    var value_benar = {
      "jawaban": $("#kategori").val() == 'teks' ? jawaban_nama_benar : $("#jawaban-gambar-benar").val(),
      "is_jawaban": 1
    }
    var value_salah = {
      "jawaban": $("#kategori").val() == 'teks' ? jawaban_nama_salah : $("#jawaban-gambar-salah").val(),
      "is_jawaban": 0
    }
    var answer_data = new Array;
    answer_data.push(value_benar);
    answer_data.push(value_salah);
    console.log(answer_data);

    var formData = {
      kategori: kategori,
      judul: judul,
      gambar: gambar,
      answers: answer_data
    }

    $.ajax({
      type: "post",
      url: "http://hoi.bigio.id/api/quiz/create",
      data: formData,
      success: function (response) {
        // $.growl.notice({
        //   message: "Data Story berhasil Ditambahkan"
        // });
        swal(`Kuis Berhasil Disimpan! `, {
          icon: "success",
        });
        $("#add-button").attr("disabled", false);
        location.replace('/kuis/tambah')
      },
      error: function (xhr, ajaxOptions, thrownError) {
        $.growl.error({
          message: "Data Gagal Ditambahkan!"
        });
        $("#add-button").attr("disabled", false);
      }
    });
  });
});

    //# sourceURL=/view/outlet/list_outlet.js