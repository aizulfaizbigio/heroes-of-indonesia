$(document).ready(function () {
    
    $('#kategori').on('change', (e) => {
        if($("#kategori").val() == 'daerah'){
            $('#region-container').show()
        }else{
            // $('#region').val("")
            $('#region-container').hide()
        }
    })

    $("#area_id").select2({
        ajax: {
            url: '/api/area/list-select2',
            type: "post",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true
        }
    });

    $('body').on('submit', '#form-add-story', function (e) {
        e.preventDefault();

        var formData = new FormData(this);
        $.ajax({
            type: "post",
            url: "/api/story/create",
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $("#add-button").attr("disabled", true);
            },
            success: function (response) {
                console.log(response);
                $.growl.notice({
                    message: `Kisah Pahlawan ${$('#nama_pahlawan').val()} berhasil ditambahkan!`
                });
                $("#add-button").attr("disabled", false);
                location.replace("/kisah");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $.growl.error({
                    message: `Kisah Pahlawan ${$('#nama_pahlawan').val()} gagal ditambahkan!`
                });
                $("#add-button").attr("disabled", false);
            }
        });
    });
});