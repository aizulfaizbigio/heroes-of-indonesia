$(document).ready(function () {
    $('#filter-category').formSelect();

    getData(
        function (response) {
            paginate(response);
            generateList(response.data);
        },
        function (error) {
            alert('kesalahan server')
        }, 1
    );

    $('#filter-search').on('keyup paste', function () {
        getData(
            function (response) {
                paginate(response);
                generateList(response.data);
            },
            function (error) {
                alert('kesalahan server')
            }, 1
        );
    });

    $('#filter-category').on('change', function () {
        getData(
            function (response) {
                paginate(response);
                generateList(response.data);
            },
            function (error) {
                alert('kesalahan server')
            }, 1
        );
    });
});

function deleteStory(event, id){
  swal({
    title: `Apakah yakin anda menghapus data ${$(event).attr('data-judul')}?`,
    text: "Data yang dihapus tidak bisa dikembalikan!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
        $.ajax({
            type: "post",
            url: "/api/story/delete",
            data: {
                id: id
            },
            dataType: "JSON",
            success: function (response) {
                console.log(response);
                swal(`Kisah Pahlawan ${$(event).attr('data-judul')} berhasil dihapus!`, {
                    icon: "success",
                });
                getData(
                    function (response) {
                        paginate(response);
                        generateList(response.data);
                    },
                    function (error) {
                        alert('kesalahan server')
                    }, 1
                );
            },
            error: function (jqXHR, textStatus, errorThrown) {
                swal(`Batal Hapus Kisah Pahlawan ${$(event).attr('data-judul')}.`);
                $(event).click()
            }
        });
      
    } else {
        swal(`Batal Hapus Kisah Pahlawan ${$(event).attr('data-judul')}.`);
    }
  });
}

function validating(event, id){
  if($(event).is(':checked')){
    $.ajax({
        type: "post",
        url: "/api/story/validation",
        data: {
            id: id
        },
        dataType: "JSON",
        success: function (response) {
            console.log(response);
            $.growl.notice({
                message: `Data "${$(event).attr('data-judul')}" Berhasil Divalidasi!`
            });
            var template_button = `
                <a href="/kisah/detail/${id}" class="waves-effect blue accent-4 btn"><i class="material-icons">remove_red_eye</i></a>
                <a href="/kisah/ubah/${id}" class="waves-effect yellow accent-4 btn"><i class="material-icons">edit</i></a>`
            $(event).closest('.card-action').find('.buttons').prepend(template_button)
            $(event).closest('.switch').delay(500).fadeOut()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.growl.error({
                message: `Data "${$(event).attr('data-judul')}" Gagal Divalidasi!`
            });
            $(event).click()
        }
    });
  }
}

function getData(onSuccess, onError, page){
    $.ajax({
        type: "post",
        url: "/api/story/datatable",
        data: {
            search_text: $("#filter-search").val(),
            kategori: $("#filter-category").val(),
            length: 6,
            page: page
        },
        dataType: "JSON",
        success: function (response) {
            // console.log(response);
            onSuccess(response);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            onError(jqXHR);
        }
    });
}

function generateList(records) {
    $('#card-container').html('');
    var row = `<div class="row">`
    var length = records.length
    $.each(records, function (index, val) {
        console.log(index)
        var temp = ``;
        var card = $("#story-card-clonable").clone().removeClass("hide");

        card.removeAttr('id');
        card.find('.logo').attr('src', val.gambar == null? DEFAULT: (DOMAIN + val.gambar))
        if(val.is_validasi){
            var temp = `<i class="material-icons verified" title="This story has been validated">verified_user</i> <span id="nama-pahlawan1">${val.nama_pahlawan}</span><i class="material-icons right">more_vert</i>`
            var action = `
                <a href="/kisah/detail/${val.id}" class="waves-effect blue accent-4 btn"><i class="material-icons">remove_red_eye</i></a>
                <a href="/kisah/ubah/${val.id}" class="waves-effect yellow accent-4 btn"><i class="material-icons">edit</i></a>
                <button class="waves-effect red darken-3 btn" onclick="deleteStory(this, ${val.id})" data-judul="${val.nama_pahlawan}"><i class="material-icons">delete_forever</i></button>`;
        }else{
            var temp = `<span id="nama-pahlawan1">${val.nama_pahlawan}</span><i class="material-icons right">more_vert</i>`
            var action = `
                <div class="switch left" style="margin-top: 5px">
                    <label>
                        <input onclick="validating(this, ${val.id})" data-judul="${val.nama_pahlawan}" type="checkbox">
                        <span class="lever"></span>
                        Validate
                    </label>
                </div>
                <div class="buttons">
                    <button class="waves-effect red darken-3 btn" onclick="deleteStory(this, ${val.id})" data-judul="${val.nama_pahlawan}"><i class="material-icons">delete_forever</i></button>
                </div>`;
        }
        card.find('.card-header').html(temp);
        card.find('.card-action').html(action);
        card.find('.card-header-detail').html(`${val.judul}<i class="material-icons right">close</i>`);
        card.find('.short-desc').html(val.cerita);
        card.find('.long-desc').html(val.cerita);
        row += card.html();
        if(length > 2){
            if(index == 2 || (length - 1) == index){
                row != `</div>`;
                $('#card-container').append(row);
                row = `<div class="row">`
            }
        }else{
            if((length - 1) == index){
                $('#card-container').append(row);
                row != `</div>`;
                row = `<div class="row">`
            }
        }
    });
    if (records.length == 0)
        $('#card-container').html('<center>Data tidak tersedia</center>');
}

function paginate(response) {
    $('#pagination').twbsPagination('destroy');
    var pagination = $('#pagination'),
        totalRecords = response.recordsTotal,
        records = response.data,
        displayRecords = [],
        recPerPage = 6,
        page = 1,
        totalPages = Math.ceil(totalRecords / recPerPage);
    
    console.log(totalPages)

    pagination.twbsPagination({
        totalPages: totalPages,
        visiblePages: 6,
        onPageClick: function (event, page) {
            // displayRecordsIndex = Math.max(page - 1, 0) * recPerPage;
            // endRec = (displayRecordsIndex) + recPerPage;
            // displayRecords = records.slice(displayRecordsIndex, endRec);
            // generate_table();
            getData(
                function (response) {
                    generateList(response.data);
                },
                function (error) {
                    alert('kesalahan server')
                }, page
            );
        }
    });
}